

    // Burger toggle
    jQuery(document).ready(function($){

        balanceText(); 
        balanceText('h1',{watch: true});
        balanceText('h2',{watch: true});
        balanceText('h3',{watch: true});
        balanceText('h4',{watch: true});
        balanceText('h5',{watch: true});

        $(".burger-wrapper").click(function() {
            console.log('hello again');
            $(this).toggleClass("active");
            $('.mobile-menu').toggleClass("active");
        });

        $('strong.schema-faq-question').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
    
            if($(this).parent().hasClass('open')){
                $('strong.schema-faq-question').parent().removeClass('open');
            } else {
                $('strong.schema-faq-question').parent().removeClass('open');
                $(this).parent().addClass('open');
            }
        });


    });



function initAcc(elem, option){
    //addEventListener on mouse click
    document.addEventListener('click', function (e) {
        //check is the right element clicked
        if (!e.target.matches(elem+' .a-btn')) return;
        else{
            //check if element contains active class
            if(!e.target.parentElement.classList.contains('active')){
                if(option==true){
                        //if option true remove active class from all other accordions
                    var elementList = document.querySelectorAll(elem +' .a-container');
                    Array.prototype.forEach.call(elementList, function (e) {
                        e.classList.remove('active');
                    });
                }
                //add active class on cliked accordion
                e.target.parentElement.classList.add('active');
            }else{
                //remove active class on cliked accordion
                e.target.parentElement.classList.remove('active');
            }
        }
    });
}

//activate accordion function
initAcc('.accordion', true);

// BalanceText
// "use strict";(function(root,factory){if(typeof define==="function"&&define.amd){define([],factory)}else if(typeof module==="object"&&module.exports){module.exports=factory()}else{root.balanceText=factory()}})(this,function(){var breakMatches,wsnwMatches,wsnwOffset;var watching={sel:[],el:[]};var handlersInitialized=false;var polyfilled=false;function noop(){return}function forEach(elements,callback){Array.prototype.forEach.call(elements,callback)}function ready(fn){if(document.readyState!=="loading"){fn()}else if(document.addEventListener){document.addEventListener("DOMContentLoaded",fn)}else{document.attachEvent("onreadystatechange",function(){if(document.readyState!=="loading"){fn()}})}}function debounce(func,threshold,execAsap){var timeout;return function debounced(){var obj=this,args=arguments;function delayed(){if(!execAsap){func.apply(obj,args)}timeout=null}if(timeout){clearTimeout(timeout)}else if(execAsap){func.apply(obj,args)}timeout=setTimeout(delayed,threshold||100)}}function hasTextWrap(){var style=document.documentElement.style;return style.textWrap||style.WebkitTextWrap||style.MozTextWrap||style.MsTextWrap}function NextWS_params(){this.reset()}NextWS_params.prototype.reset=function(){this.index=0;this.width=0};function isWhiteSpaceNoWrap(index){return wsnwMatches.some(function(range){return range.start<index&&index<range.end})}function recursiveCalcNoWrapOffsetsForLine(el,includeTag){if(el.nodeType===el.ELEMENT_NODE){var style=window.getComputedStyle(el);if(style.whiteSpace==="nowrap"){var len=el.outerHTML.length;wsnwMatches.push({start:wsnwOffset,end:wsnwOffset+len});wsnwOffset+=len}else{forEach(el.childNodes,function(child){recursiveCalcNoWrapOffsetsForLine(child,true)});if(includeTag){wsnwOffset+=el.outerHTML.length-el.innerHTML.length}}}else if(el.nodeType===el.COMMENT_NODE){wsnwOffset+=el.length+7}else if(el.nodeType===el.PROCESSING_INSTRUCTION_NODE){wsnwOffset+=el.length+2}else{wsnwOffset+=el.length}}function calcNoWrapOffsetsForLine(el,oldWS,lineCharOffset){if(lineCharOffset===0){el.style.whiteSpace=oldWS;wsnwOffset=0;wsnwMatches=[];recursiveCalcNoWrapOffsetsForLine(el,false);el.style.whiteSpace="nowrap"}else{var newMatches=[];wsnwMatches.forEach(function(match){if(match.start>lineCharOffset){newMatches.push({start:match.start-lineCharOffset,end:match.end-lineCharOffset})}});wsnwMatches=newMatches}}function removeTags(el){var brs=el.querySelectorAll('br[data-owner="balance-text-hyphen"]');forEach(brs,function(br){br.outerHTML=""});brs=el.querySelectorAll('br[data-owner="balance-text"]');forEach(brs,function(br){br.outerHTML=" "});var spans=el.querySelectorAll('span[data-owner="balance-text-softhyphen"]');if(spans.length>0){forEach(spans,function(span){var textNode=document.createTextNode("Â­");span.parentNode.insertBefore(textNode,span);span.parentNode.removeChild(span)})}spans=el.querySelectorAll('span[data-owner="balance-text-justify"]');if(spans.length>0){var txt="";forEach(spans,function(span){txt+=span.textContent;span.parentNode.removeChild(span)});el.innerHTML=txt}}var isJustified=function(el){var style=el.currentStyle||window.getComputedStyle(el,null);return style.textAlign==="justify"};function justify(el,txt,conWidth){var div,size,tmp,words,wordSpacing;txt=txt.trim();words=txt.split(" ").length;txt=txt+" ";if(words<2){return txt}tmp=document.createElement("span");tmp.innerHTML=txt;el.appendChild(tmp);size=tmp.offsetWidth;tmp.parentNode.removeChild(tmp);wordSpacing=Math.floor((conWidth-size)/(words-1));tmp.style.wordSpacing=wordSpacing+"px";tmp.setAttribute("data-owner","balance-text-justify");div=document.createElement("div");div.appendChild(tmp);return div.innerHTML}function isBreakChar(txt,index){var re=/(\s|-|\u2014|\u2013|\u00ad)(?![^<]*>)/g,match;if(!breakMatches){breakMatches=[];match=re.exec(txt);while(match!==null){if(!isWhiteSpaceNoWrap(match.index)){breakMatches.push(match.index)}match=re.exec(txt)}}return breakMatches.indexOf(index)!==-1}function isBreakOpportunity(txt,index){return index===0||index===txt.length||isBreakChar(txt,index-1)&&!isBreakChar(txt,index)}function findBreakOpportunity(el,txt,conWidth,desWidth,dir,c,ret){var w;if(txt&&typeof txt==="string"){for(;;){while(!isBreakOpportunity(txt,c)){c+=dir}el.innerHTML=txt.substr(0,c);w=el.offsetWidth;if(dir<0){if(w<=desWidth||w<=0||c===0){break}}else{if(desWidth<=w||conWidth<=w||c===txt.length){break}}c+=dir}}ret.index=c;ret.width=w}function getSpaceWidth(el,h){var dims,space,spaceRatio,container=document.createElement("div");container.style.display="block";container.style.position="absolute";container.style.bottom=0;container.style.right=0;container.style.width=0;container.style.height=0;container.style.margin=0;container.style.padding=0;container.style.visibility="hidden";container.style.overflow="hidden";space=document.createElement("span");space.style.fontSize="2000px";space.innerHTML="&nbsp;";container.appendChild(space);el.appendChild(container);dims=space.getBoundingClientRect();container.parentNode.removeChild(container);spaceRatio=dims.height/dims.width;return h/spaceRatio}function getElementsList(elements){if(!elements){return[]}if(typeof elements==="string"){return document.querySelectorAll(elements)}if(elements.tagName&&elements.querySelectorAll){return[elements]}return elements}function balanceText(elements){forEach(getElementsList(elements),function(el){var maxTextWidth=5e3;removeTags(el);var oldWS=el.style.whiteSpace,oldFloat=el.style.float,oldDisplay=el.style.display,oldPosition=el.style.position,oldLH=el.style.lineHeight;el.style.lineHeight="normal";var containerWidth=el.offsetWidth,containerHeight=el.offsetHeight;el.style.whiteSpace="nowrap";el.style.float="none";el.style.display="inline";el.style.position="static";var nowrapWidth=el.offsetWidth,nowrapHeight=el.offsetHeight,spaceWidth=oldWS==="pre-wrap"?0:getSpaceWidth(el,nowrapHeight);if(containerWidth>0&&nowrapWidth>containerWidth&&nowrapWidth<maxTextWidth){var remainingText=el.innerHTML,newText="",lineText="",shouldJustify=isJustified(el),totLines=Math.round(containerHeight/nowrapHeight),remLines=totLines,lineCharOffset=0;var desiredWidth,guessIndex,le,ge,splitIndex,isHyphen,isSoftHyphen;while(remLines>1){breakMatches=null;calcNoWrapOffsetsForLine(el,oldWS,lineCharOffset);desiredWidth=Math.round((nowrapWidth+spaceWidth)/remLines-spaceWidth);guessIndex=Math.round((remainingText.length+1)/remLines)-1;le=new NextWS_params;findBreakOpportunity(el,remainingText,containerWidth,desiredWidth,-1,guessIndex,le);ge=new NextWS_params;guessIndex=le.index;findBreakOpportunity(el,remainingText,containerWidth,desiredWidth,+1,guessIndex,ge);le.reset();guessIndex=ge.index;findBreakOpportunity(el,remainingText,containerWidth,desiredWidth,-1,guessIndex,le);if(le.index===0){splitIndex=ge.index}else if(containerWidth<ge.width||le.index===ge.index){splitIndex=le.index}else{splitIndex=Math.abs(desiredWidth-le.width)<Math.abs(ge.width-desiredWidth)?le.index:ge.index}lineText=remainingText.substr(0,splitIndex).replace(/\s$/,"");isSoftHyphen=Boolean(lineText.match(/\u00ad$/));if(isSoftHyphen){lineText=lineText.replace(/\u00ad$/,'<span data-owner="balance-text-softhyphen">-</span>')}if(shouldJustify){newText+=justify(el,lineText,containerWidth)}else{newText+=lineText;isHyphen=isSoftHyphen||Boolean(lineText.match(/(-|\u2014|\u2013)$/));newText+=isHyphen?'<br data-owner="balance-text-hyphen" />':'<br data-owner="balance-text" />'}remainingText=remainingText.substr(splitIndex);lineCharOffset=splitIndex;remLines--;el.innerHTML=remainingText;nowrapWidth=el.offsetWidth}if(shouldJustify){el.innerHTML=newText+justify(el,remainingText,containerWidth)}else{el.innerHTML=newText+remainingText}}el.style.whiteSpace=oldWS;el.style.float=oldFloat;el.style.display=oldDisplay;el.style.position=oldPosition;el.style.lineHeight=oldLH})}function updateWatched(){var selectors=watching.sel.join(","),selectedElements=getElementsList(selectors),elements=Array.prototype.concat.apply(watching.el,selectedElements);balanceText(elements)}function initHandlers(){if(handlersInitialized){return}ready(updateWatched);window.addEventListener("load",updateWatched);window.addEventListener("resize",debounce(updateWatched));handlersInitialized=true}function balanceTextAndWatch(elements){if(typeof elements==="string"){watching.sel.push(elements)}else{forEach(getElementsList(elements),function(el){watching.el.push(el)})}initHandlers();updateWatched()}function unwatch(elements){if(typeof elements==="string"){watching.sel=watching.sel.filter(function(el){return el!==elements})}else{elements=getElementsList(elements);watching.el=watching.el.filter(function(el){return elements.indexOf(el)===-1})}}function polyfill(){if(polyfilled){return}watching.sel.push(".balance-text");initHandlers();polyfilled=true}function publicInterface(elements,options){if(!elements){polyfill()}else if(options&&options.watch===true){balanceTextAndWatch(elements)}else if(options&&options.watch===false){unwatch(elements)}else{balanceText(elements)}}publicInterface.updateWatched=updateWatched;if(hasTextWrap()){noop.updateWatched=noop;return noop}return publicInterface});

function sizeCheckers () {

    jQuery( "iframe" ).each(function() {
        var attr = jQuery(this).attr('data-originalheight');
        if (attr) {
        } else {
            var originalheight = jQuery(this).attr('height');
            var originalwidth = jQuery(this).attr('width');
            jQuery(this).attr('data-originalheight',originalheight);
            jQuery(this).attr('data-originalwidth',originalwidth);
        }
        jQuery(this).width('100%');
        var format = parseInt(jQuery(this).attr('data-originalheight'))/parseInt(jQuery(this).attr('data-originalwidth'));
        var newwidth = jQuery(this).outerWidth();
        var newheight = newwidth * format;
        jQuery(this).height(newheight);
    });
}

function calculateTotals() {
    var currentUnitPrice = parseInt(jQuery('span#showprice').text());
    var currentUnits = parseInt(jQuery('select#units').val());
    var unitsTotal = currentUnitPrice * currentUnits;

    var extras = jQuery('.gfield.extras').find('select').find(":selected").val();
    var extrasArray = extras.split("|");
    var extrasPrice = parseInt(extrasArray[1]);
    var extrasApplications = extrasArray[0];
    if(!!extrasArray[0]){
    } else {
        var extrasApplications = '-';
    }

    

    var allTotal = unitsTotal + extrasPrice;
    jQuery('.summary .totalt .calculations').text(allTotal+' kr / månad');

    var offerValues = 0
    jQuery( ".summary .items .sub-items li" ).each(function( index ) {
        offerValues = offerValues + parseInt(jQuery(this).find('del').text());
    });
    var valuesTotal = offerValues;
    jQuery('.summary .rabatter .calculations').text('- ' + valuesTotal+' kr / månad');

    var appInfo = '';
    jQuery( ".gfield.extras .ginput_container input.appinfo" ).each(function( index ) {
        var no = jQuery(this).attr('data-no');
        var apptext = jQuery(this).val();
        appInfo = appInfo+'App '+no+': '+apptext+'\r\n';
    });


    jQuery('.gfield.digmi_units .ginput_container').find('textarea').html(currentUnits+' enheter\r\n'+currentUnitPrice+' kr per enhet\r\nTotalt '+unitsTotal+' kr per månad');
    jQuery('.gfield.digmi_addons .ginput_container').find('textarea').html(extrasApplications+' applikationer\r\nTotalt '+extrasPrice + ' kr per månad\r\n\r\n'+appInfo);
    jQuery('.gfield.digmi_prisinfo .ginput_container').find('textarea').html('Totalt: '+allTotal+' kr per månad');
}



function updatePrice() {
    jQuery('span#showprice').addClass('updating');
    var units = jQuery('select#units').val();
    jQuery.ajax({
        type: "post",
        dataType: "text",
        url: my_ajax_object.ajax_url,
        data : {action: "get_price", units : units},
        success: function(msg){
            //console.log(msg);
            jQuery('span#showprice').text(msg);
            jQuery('#unitpricesummary').text(msg + ' kr');
            jQuery('span#showprice').removeClass('updating');
            jQuery('.selectholder').html('');
            jQuery('.selectholder').prepend('Antal enheter:');
            jQuery( "select#units" ).clone().appendTo( ".selectholder" );

            var value = jQuery( "select#units" ).find(":selected").val();
            jQuery(".selectholder select").val(value).change();
            jQuery( ".selectholder select#units" ).attr('id','unitsselector');

            calculateTotals();
        }
    });
}


jQuery(document).ready(function($) {

        sizeCheckers();
        updatePrice();

        $(window).resize(function() {
            sizeCheckers();
        });

        $(".openbooking a").on("click",function(event) {
            Calendly.showPopupWidget('https://calendly.com/digmi/bokademo?hide_gdpr_banner=1&primary_color=f19b06');return false;
            event.preventDefault();
        });
        $("a.openbooking").on("click",function(event) {
            Calendly.showPopupWidget('https://calendly.com/digmi/bokademo?hide_gdpr_banner=1&primary_color=f19b06');return false;
            event.preventDefault();
        });


        $( "a.wp-block-button__link svg" ).each(function( index ) {
            var viewBox = $(this).attr('viewBox');
            if (typeof viewBox == 'undefined' || viewBox == false) {
                var width = $(this).attr('width');
                var height = $(this).attr('height');
                $(this).attr('viewBox','0 0 ' + width + ' ' + height);
            }
        });

        // Init Balancetext
        // balanceText('.h1');

      var originalunits = $('.units-wrapper select').val();
      //console.log('originalunits: '+originalunits);
      $('#input_1_22_1').val(originalunits);

      $( ".gfield.extras .gfield_label, .gfield.extras .gfield_description" ).wrapAll( "<div class='new' />");
      $( ".gfield.extras .new" ).wrapAll( "<div class='digmi-wrapper' />");

      $('.extras .digmi-wrapper').prepend('<div class="checkbox"><label><input type="checkbox" class="activate" /></label></div>');

      var extrapris = $('.gfield.extras').find('select option:eq(1)').val();
      const myArray = extrapris.split("|");
      $('.extras .digmi-wrapper').append('<div class="pricing"><span>'+myArray[1]+ 'kr</span>per applikation / månad</div>');

      var submitID = $( ".gform_footer input[type=submit]" ).attr('id');
      $( ".gform_footer input[type=submit]" ).clone().appendTo( ".summaryfooter" );
      $( ".summaryfooter input[type=submit]" ).attr('id','');
      $( ".summaryfooter input[type=submit]" ).attr('onclick','');
      $( ".summaryfooter input[type=submit]" ).attr('onkeypress','');
      $( ".summaryfooter input[type=submit]" ).attr('data-id',submitID);

      $('body').on('click', '.summaryfooter input[type=submit]', function() {
        $( ".gform_wrapper" ).find('form').submit();
        $( ".form-wrapper .summary" ).hide('slow');
    });

        $("button.get-recommended").on("click",function() {
            $('section.form').addClass('active');
        });
        $("button.close.recommended").on("click",function() {
            $('section.form').removeClass('active');
        });


        $("header#header_wrapper .mobile-toggle button").on("click",function() {
            $(this).toggleClass('active');
            $('header#header_wrapper .menu-item').toggleClass('mobile-active');
        });


        $("button.increse").on("click",function() {
            var currentval = parseInt($('#units').val());
            var maxval =  parseInt($('#units').attr('max'));
            if(currentval<maxval){
                var newval = currentval+1;
                $('#units').val(newval);
            } else {
                $('#units').val(maxval);
            }
        });
        $("button.decrese").on("click",function() {
            var currentval = parseInt($('#units').val());
            var minval =  parseInt($('#units').attr('min'));
            if(currentval>minval){
                var newval = currentval-1;
                $('#units').val(newval);
            } else {
                $('#units').val(minval);
            }
        });



        $('select#units').on("change", function() {
            updatePrice();
        });

        $('body').on('change', 'select#unitsselector', function() {
            var newval = $(this).val();
            $('select#units').val(newval);
            $('select#units').change();
        });

/*         $(document).on('click', '.gfield.extras .digmi-wrapper .checkbox label', function() {
            if ($(this).hasClass("active")) {
                $('.gfield.extras .ginput_container select option:eq(0)').attr('selected', 'selected');
                $('.gfield.extras .ginput_container select').val($('.gfield.extras .ginput_container select option:eq(0)').val()).change();
                $(this).removeClass("active");
            } else {
                $('.gfield.extras .ginput_container select option:eq(1)').attr('selected', 'selected');
                $('.gfield.extras .ginput_container select').val($('.gfield.extras .ginput_container select option:eq(1)').val()).change();
                $(this).addClass("active");
            }
        }); */

         $(document).on('change', '.gfield.extras .ginput_container select', function() {
            var currentVal = $(this).val();
            const valArray = currentVal.split("|");

            if(parseInt(valArray[1]) > 0) {
                $(this).parent().parent().find('.digmi-wrapper .checkbox label').addClass('active');
            } else {
                $(this).parent().parent().find('.digmi-wrapper .checkbox label').removeClass('active');

            }

            var noApps = parseInt(valArray[0]);
            if(!!noApps) {
            } else {
                noApps = parseInt(0);
            }
            var i = 0;
            while (i < noApps) {
                i++;
                if ($('.gfield.extras .ginput_container input.app-'+i).length < 1) {
                    $('.gfield.extras .ginput_container').append('<input type="text" name="appinfo-'+i+'" class="appinfo app-'+i+'" data-no="'+i+'" placeholder="Namn på app '+i+'" />');
                }
            }

            $( ".gfield.extras .ginput_container input.appinfo" ).each(function( index ) {
                var no = parseInt($(this).attr('data-no'));
                if(no > i) {
                    $(this).remove();
                }
            });

            calculateTotals();
        });

        $(document).on('change', '.gfield.extras .ginput_container input.appinfo', function() {
            calculateTotals();
        });

        // Social share
        $(".nss-facebook a").prepend("<span>Dela på </span>");
        $(".nss-linkedin a").prepend("<span>Dela på </span>");
});