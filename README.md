# PARCEL WORDPRESS COMPILER

Detta är en uppsättning av ParcelJS och Wordpress. ParcelJS är en sass, javascript kompilerare med inbyggd hot reload.

### SETUP

- Includera package.json filen i tema-mappen.
- Därefter kör du "npm install" kommandot för att installera allt nödvändigt.
- Skapa en mapp /dev/ och filerna "scripts.js", "styles.scss". I scripts.js kopiera in överst "import './styles.scss';".
- Därefter kör kommandot "npm run watch" för att starta. Initalt skapas då mappen /watch/ om den inte redan finns.
- ParcelJS kompilerar sass och javascripten samt håller koll om någon annan fil har modifierats och laddar om webbläsaren.
- Inkludera filerna från /watch/ mappen i "functions.php" som vanligt.

### COMMANDS

- "npm run watch" - watch files and output them in /watch/ folder
- "npm run build" - build files, minify and output them in /build/ folder

### DEV FOLDER

- The /dev/ folder is where you hold your css and javascript -files for development
