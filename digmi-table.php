<?php
    $business = get_field('business');
    $business_table = $business['business_table'];

    $other = get_field('other');
    $other_table = $other['other_table'];
?>

<div class="alignfull has-lightgrey-background-color desktop" style="width: 100%;">
    <div class="table-wrapper">
        <div class="table-wrapper-top">
            <div class="col empty"></div>
            <div class="col orange">
                <h5>Standard</h5>
                <p>Bästa lösningen för din växande verksamhet</p>
            </div>
            <div class="col black">
                <h5>Anpassad</h5>
                <p>Smidiga lösningar för att ta din verksamhet till nästa steg</p>
            </div>
        </div>
        <h4>Anpassad för företag</h4>
        <section>
            <?php foreach( $business_table as $business_table_row ): ?>
                <div class="table-row">
                    <div class="table-item title">
                        <?php echo $business_table_row['title']; ?>
                    </div>
                    <div class="table-item what">
                        <?php if($business_table_row['deal']['orange_check']) { ?>
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/check-orange.svg" alt="Check">
                        <?php } ?>
                        <?php if($business_table_row['deal']['gray_x']) { ?>
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/x.svg" alt="Nothing">
                        <?php } ?>

                        <?php echo $business_table_row['deal']['text']; ?>
                    </div>
                    <div class="table-item what">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/check-orange.svg" alt="nothign">
                    </div>
                </div>
            <?php endforeach; ?>
        </section>
        <h4>Övrigt</h4>
        <section>
            <?php foreach( $other_table as $other_table_row ): ?>
                <div class="table-row">
                    <div class="table-item title">
                        <?php echo $other_table_row['title']; ?>
                    </div>
                    <div class="table-item what">
                        <?php
                            foreach( $other_table_row['deal']['orange_check'] as $orange_check ):
                                if($orange_check === 'yes') {
                                    ?>
                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/check-orange.svg" alt="nothign">
                                    <?php
                                } else {
                                    echo "";
                                }
                            endforeach;
                            foreach( $other_table_row['deal']['gray_x'] as $gray_x ):
                                if($gray_x === 'yes') {
                                    ?>
                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/x.svg" alt="nothign">
                                    <?php
                                } else {
                                    echo "";
                                }
                            endforeach;
                            echo $other_table_row['deal']['text'];
                        ?>
                    </div>
                    <div class="table-item what">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/check-orange.svg" alt="nothign">
                    </div>
                </div>
            <?php endforeach; ?>
        </section>
    </div>
</div>

<div class="alignfull has-lightgrey-background-color mobile">
    <div class="table-wrapper">
        <h4>Standard</h4>
        <p>Anpassad för företag</p>
        <section class="em">
            <?php foreach( $business_table as $business_table_row ): ?>
                <div class="table-row">
                    <div class="table-item what">
                        <?php echo $business_table_row['title']; ?>
                        <?php if($business_table_row['deal']['orange_check']) { ?>
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/check-orange.svg" alt="Check">
                        <?php } ?>
                        <?php if($business_table_row['deal']['gray_x']) { ?>
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/x.svg" alt="Nothing">
                        <?php } ?>

                        <?php echo $business_table_row['deal']['text']; ?>
                    </div>
                    <!-- <div class="table-item what">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/check-orange.svg" alt="nothign">
                    </div> -->
                </div>
            <?php endforeach; ?>
        </section>
        <h5>Övrigt</h5>
        <section>
            <?php foreach( $other_table as $other_table_row ): ?>
                <div class="table-row">
                    <div class="table-item what">
                        <?php echo $other_table_row['title']; ?>
                        <?php
                            foreach( $other_table_row['deal']['orange_check'] as $orange_check ):
                                if($orange_check === 'yes') {
                                    ?>
                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/check-orange.svg" alt="nothign">
                                    <?php
                                } else {
                                    echo "";
                                }
                                // echo $orange_check;
                            endforeach;
                            foreach( $other_table_row['deal']['gray_x'] as $gray_x ):
                                if($gray_x === 'yes') {
                                    ?>
                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/x.svg" alt="nothign">
                                    <?php
                                } else {
                                    echo "";
                                }
                            endforeach;
                            echo $other_table_row['deal']['text'];
                        ?>
                    </div>
                    <!-- <div class="table-item what">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/check-orange.svg" alt="nothign">
                    </div> -->
                </div>
            <?php endforeach; ?>
        </section>
        <h5>Anpassad</h5>
        <section>
            <p>All your essential business finsajkdapfwq</p>
        </section>
    </div>
</div>