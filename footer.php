<?php
    $social_links = get_field('social_links', 'options');
?>
<div id="footer_wrapper" class="big-wrapper footer-wrapper">
    <div class="outer footer-wrapper">
        <footer>
            <div class="wrapper">
                <div class="inner">
                    <div class="col">
                        <div class="row">
                            <a href="/" class="logotype">
                                <img src="<?php echo bloginfo('template_url'); ?>/assets/images/digmi-logotype-black.svg" alt="digmi logo">
                            </a>
                        </div>
                        <div class="row">
                            <div class="sub-col"><?php the_field('kontaktuppgifter', 'options'); ?></div>
                            <div class="sub-col"><?php the_field('kontaktuppgifter_2', 'options'); ?></div>
                            <div class="sub-col"><?php the_field('kontaktuppgifter_3', 'options'); ?></div>
                        </div>
                        <div class="row">
                            <ul class="social-links">
                                <?php foreach( $social_links as $social ): ?>
                                    <li>
                                        <a target="_blank" href="<?php echo $social['link']; ?>">
                                            <img src="<?php echo $social['icon']; ?>" alt="social länk">
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row footer-nav">
                            <div class="sub-col footer-menu-1">
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'footer_menu',
                                        'container_class' => 'footer-menu'
                                    ) );
                                ?>
                            </div>
                            <div class="sub-col footer-menu-2">
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'footer_menu_2',
                                        'container_class' => 'footer-menu'
                                    ) );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>

<!-- Form wrapper -->
<section class="form recommended">

<nav>
    <div class="nav-wrapper">
        <button class="close recommended">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/button-arrow.svg" height="20" width="20" alt="Tillbaka">
            Tillbaka
        </button>
    </div>
</nav>


<div class="form-wrapper">
    <div id="theform">
        <?php echo do_shortcode('[gravityform id="7" title="false" description="false" ajax="true" tabindex="49"]'); ?>
    </div>
    <div class="summary">
        <h2>Sammanfattning</h2>
        <div class="inside">
            <ul class="items">
                <li>
                    <h3><?php the_title(); ?></h3>
                    <div class="summaryselector">
                        <p><span id="unitpricesummary"></span> per enhet / månad</p>
                        <div class="selectholder"></div>
                    </div>

                    <?php if( have_rows('erbjudanden') ): ?>
                        <ul class="sub-items">
                        <?php while( have_rows('erbjudanden') ): the_row();
                            ?>
                            <li>
                                <p>
                                    <?php the_sub_field('rubrik'); ?><br/>
                                    <del <?php if(get_sub_field('ord_pris') == '0') {echo ' class="hide"';}?>><?php the_sub_field('ord_pris'); ?> kr</del> <strong>0 kr</strong> <span>/ månad</span>
                                </p>
                            </li>
                        <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>

                </li>
                <!-- <li>Till채gg</li> -->
                <li class="summary">
                    <div class="rabatter">
                        <p>Rabatter:</p>
                        <p class="calculations">- 239 kr / månad</p>
                    </div>
                    <div class="totalt">
                        <p>Total kostnad:</p>
                        <p class="calculations"></p>
                    </div>
                </li>
                <li>
                    Avtalstiden är 36 månader
                </li>
                <li class="summaryfooter">

                </li>
        </div>
    </div>
</div>
</section>
<?php wp_footer(); ?>
</body>
