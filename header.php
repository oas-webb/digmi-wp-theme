<!DOCTYPE html>
<html lang="sv">
<head>

<!--

   ___   __ _ ___   _ __  _   _
  / _ \ / _` / __| | '_ \| | | |
 | (_) | (_| \__ \_| | | | |_| |
  \___/ \__,_|___(_)_| |_|\__,_|

-->
<?php if (WP_ENV == 'production' || WP_ENV == 'staging'){ ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WNDSZKW');</script>
<!-- End Google Tag Manager -->
<?php } ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title();?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php if (WP_ENV == 'production' || WP_ENV == 'staging'){ ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WNDSZKW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php } ?>

<header id="header_wrapper" class="big-wrapper header-wrapper">
    <div class="wrapper">
        <div class="outer">
            <nav class="inner">

                <ul>
                    <li>
                        <a href="/" class="logotype">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/digmi-logotype.svg" alt="digmi logo">
                        </a>
                    </li>

                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'digmi_menu',
                            'container' => false,
                        ) );
                    ?>

                    <li class="button-wrapper">
                        <?php
                        $menu_locations = get_nav_menu_locations();
                        $menuid = $menu_locations['digmi_menu'];
                        $menu = wp_get_nav_menu_object($menuid);
                        ?>
                        <?php if(get_field('knapplank_2', $menu)) {
                            $buttonlink = get_field('knapplank_2', $menu);
                            $link_target = $buttonlink['target'] ? $buttonlink['target'] : '_self';
                            ?>
                            <a class="button outline openbooking">
                                <span><?php echo $buttonlink['title']; ?></span>
                            </a>
                        <?php } ?>
                        <?php if(get_field('knapplank_1', $menu)) {
                            $buttonlink = get_field('knapplank_1', $menu);
                            $link_target = $buttonlink['target'] ? $buttonlink['target'] : '_self';
                            ?>
                            <a href="<?php echo $buttonlink['url']; ?>" target="<?php echo $link_target; ?>" class="button primary">
                                <span><?php echo $buttonlink['title']; ?></span>
                                <svg width="13" height="12" viewBox="0 0 13 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="white" stroke="white" stroke-width="0.25"/>
                                </svg>
                            </a>
                        <?php } ?>
                    </li>

                    <li class="mobile-toggle">
                            <button>
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>