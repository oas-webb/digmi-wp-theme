<div class="big-wrapper">
    <div class="outer background-black">
        <div class="wrapper">
            <div class="inner">
                <div class="digmi-wrapper">
                    <div id="digmi-offer-wrapper">
                        <div class="digmi-wrapper">
                            <div class="choice_information">
                                <div class="inside-wrapper">
                                    <h1>Speciella priser för dig</h1>
                                    <p class="text-white align-center">Tydliga paket med fasta priser och utan dolda avgifter.</p>
                                    <div class="pricing-tiers">
                                        <div class="recommended">
                                            <h5>
                                                <?php //echo get_the_title($current_id); ?>
                                                STANDARD
                                            </h5>
                                            <div class="price-wrapper">
                                                <div class="price h2"><span id="showprice"></span> kr</div>
                                                <div class="price-info">
                                                    <span class="has-black-color">enhet / månad</span>
                                                    <span class="has-digmicopy-color">faktureras kvartalsvis</span>
                                                </div>
                                            </div>
                                            <div class="units-wrapper">
                                                <!-- Check rows exists. -->
                                                <?php if( have_rows('priser','option') ):
                                                    $min = 999;
                                                    $max = 0;
                                                    while( have_rows('priser','option') ) : the_row();
                                                    $fran = (int) get_sub_field('fran');
                                                    $till = (int) get_sub_field('till');
                                                        if($fran < $min) {
                                                            $min = $fran;
                                                        }
                                                        if($till > $max) {
                                                            $max = $till;
                                                        }
                                                    endwhile;
                                                    echo '<select name="units" id="units">';
                                                    $index = 0;
                                                    foreach (range($min, $max) as $units) {
                                                        if($index == 2) {
                                                            echo '<option value="'.$units.'" selected>'.$units.' st</option>';
                                                        } else {
                                                            echo '<option value="'.$units.'">'.$units.' st</option>';
                                                        }
                                                        $index++;
                                                    }
                                                    echo '</select>';
                                                endif; ?>
                                                <label for="units">enheter</label>
                                            </div>
                                            <div class="button-wrapper">
                                                <button class="get-recommended button primary">
                                                    <span>
                                                        Köp standard
                                                    </span>
                                                </button>
                                            </div>
                                            <ul class="usp has-black-color">
                                                <li>
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7071 0.292893C14.0976 0.683417 14.0976 1.31658 13.7071 1.70711L5.70711 9.70711C5.31658 10.0976 4.68342 10.0976 4.29289 9.70711L0.292893 5.70711C-0.0976311 5.31658 -0.0976311 4.68342 0.292893 4.29289C0.683417 3.90237 1.31658 3.90237 1.70711 4.29289L5 7.58579L12.2929 0.292893C12.6834 -0.0976311 13.3166 -0.0976311 13.7071 0.292893Z" fill="#24262B"/>
                                                    </svg>
                                                    Färdiginstallerad läsplatta
                                                </li>
                                                <li>
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7071 0.292893C14.0976 0.683417 14.0976 1.31658 13.7071 1.70711L5.70711 9.70711C5.31658 10.0976 4.68342 10.0976 4.29289 9.70711L0.292893 5.70711C-0.0976311 5.31658 -0.0976311 4.68342 0.292893 4.29289C0.683417 3.90237 1.31658 3.90237 1.70711 4.29289L5 7.58579L12.2929 0.292893C12.6834 -0.0976311 13.3166 -0.0976311 13.7071 0.292893Z" fill="#24262B"/>
                                                    </svg>
                                                    Tidningsprenumeration
                                                </li>
                                                <li>
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7071 0.292893C14.0976 0.683417 14.0976 1.31658 13.7071 1.70711L5.70711 9.70711C5.31658 10.0976 4.68342 10.0976 4.29289 9.70711L0.292893 5.70711C-0.0976311 5.31658 -0.0976311 4.68342 0.292893 4.29289C0.683417 3.90237 1.31658 3.90237 1.70711 4.29289L5 7.58579L12.2929 0.292893C12.6834 -0.0976311 13.3166 -0.0976311 13.7071 0.292893Z" fill="#24262B"/>
                                                    </svg>
                                                    Koppling mot er bokningssida
                                                </li>
                                                <li>
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7071 0.292893C14.0976 0.683417 14.0976 1.31658 13.7071 1.70711L5.70711 9.70711C5.31658 10.0976 4.68342 10.0976 4.29289 9.70711L0.292893 5.70711C-0.0976311 5.31658 -0.0976311 4.68342 0.292893 4.29289C0.683417 3.90237 1.31658 3.90237 1.70711 4.29289L5 7.58579L12.2929 0.292893C12.6834 -0.0976311 13.3166 -0.0976311 13.7071 0.292893Z" fill="#24262B"/>
                                                    </svg>
                                                    App-paket Sociala medier
                                                </li>
                                                <li>
                                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7071 0.292893C14.0976 0.683417 14.0976 1.31658 13.7071 1.70711L5.70711 9.70711C5.31658 10.0976 4.68342 10.0976 4.29289 9.70711L0.292893 5.70711C-0.0976311 5.31658 -0.0976311 4.68342 0.292893 4.29289C0.683417 3.90237 1.31658 3.90237 1.70711 4.29289L5 7.58579L12.2929 0.292893C12.6834 -0.0976311 13.3166 -0.0976311 13.7071 0.292893Z" fill="#24262B"/>
                                                    </svg>
                                                    Försäkring mot stöld och skada (självrisk 2000 kr)
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="custom">
                                            <h5 class="has-white-color">Anpassad</h5>
                                            <p class="has-white-color">Skapa ett paket med det innehåll din verksamhet behöver.<br>
                                                <span class="has-digmicopy-neg-color">Vi kan anpassa enheten helt efter din salongs behov. Direktkoppling mot ert boknings-och betalningssystem, egen designat utseende, valfria appar och egna block med innehållet ni vill visa kunder.</span>
                                            </p>
                                            <div class="button-wrapper">
                                                <a href="https://digmi.se/kontakt/" target="_blank" class="button primary recommended">
                                                    <span>
                                                        Kontakta säljteamet
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>