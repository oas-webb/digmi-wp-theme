<?php
    $text_cta = get_field('text_cta');
?>

<div id="text-cta-wrapper" class="big-wrapper block-wrapper <?php echo $text_cta['background_color']; ?>" style="width: <?php echo $text_cta['width'] ?>%;">
    <div class="outer text-cta">
        <div class="wrapper">
            <div class="inner">

                <?php if($text_cta['pre_heading']): ?>
                    <h3><?php echo $text_cta['pre_heading'] ?></h3>
                <?php endif; ?>

                <?php if($text_cta['heading']): ?>
                    <h2 class="h1"><?php echo $text_cta['heading'] ?></h2>
                <?php endif; ?>

                <?php if($text_cta['text']): ?>
                    <?php echo $text_cta['text'] ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>