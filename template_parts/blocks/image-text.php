<?php
    $image_text = get_field('image_text');
    $buttons = $image_text['buttons']['button'];
    $usps = $image_text['usps']['usps'];
?>
<div id="image_text_wrapper" class="block-wrapper <?php echo $image_text['background_color']; ?>">
    <div class="wrapper">
        <div class="inner">
            <div class="col left <?php echo $image_text['image_variant']; ?>">
                <div class="inner-left">
                    <?php if($image_text['pre_heading']): ?>
                        <h3><?php echo $image_text['pre_heading']; ?></h3>
                    <?php endif; ?>
                    <h2 class="h1"><?php echo $image_text['heading']; ?></h2>
                    <div class="text-wrapper">
                        <?php echo $image_text['text']; ?>
                    </div>
                    <?php if($buttons): ?>
                        <div class="button-wrapper">
                            <?php foreach( $buttons as $button ): ?>
                                <a target="<?php echo $button['link']['target']; ?>" href="<?php echo $button['link']['url']; ?>" class="button <?php echo $button['variant']; ?>">
                                    <span class="<?php echo $button['color']; ?>" ><?php echo $button['link']['title']; ?></span>
                                    <?php if($button['icon'] === true): ?>
                                        <svg width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="<?php echo $button['icon_color']; ?>" stroke-width="0.25"/>
                                        </svg>
                                    <?php endif; ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if($image_text['social_share_buttons'] === true) {
                        naked_social_share_buttons();
                    } ?>
                </div>
                <div class="inner-right">
                    <?php if($image_text['usps']['usps']): ?>
                        <div class="usps-wrapper">
                            <?php foreach( $usps as $usp ): ?>
                                <div class="usp">
                                    <strong><?php echo $usp['heading'] ?></strong>
                                    <?php echo $usp['text'] ?>
                                    <?php if($usp['buttons']['button'][0]): ?>

                                        <div class="button-wrapper">
                                            <a class="button <?php echo $usp['buttons']['button'][0]['variant']; ?>" href="/">
                                                <span class="<?php echo $usp['buttons']['button'][0]['color']; ?>"><?php echo $usp['buttons']['button'][0]['link']['title']; ?></span>
                                                <svg width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="<?php echo $usp['buttons']['button'][0]['icon_color']; ?>" stroke-width="0.25"/>
                                                </svg>
                                            </a>
                                        </div>

                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php if($image_text['image']): ?>
                <div class="col right <?php echo $image_text['image_variant']; ?>">
                    <img class="<?php echo $image_text['rounded_corners']; ?>" src="<?php echo $image_text['image']['url']; ?>" alt="<?php echo $image_text['image']['alt']; ?>">
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>