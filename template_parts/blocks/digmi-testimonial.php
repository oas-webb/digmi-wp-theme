<?php

/**
 * Testimonial Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'digmi-testimonial-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'digmi-testimonial';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}


?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php if( have_rows('utlatanden') ): ?>

        <div class="swiper swiper-<?php echo esc_attr($id); ?>">
            <div class="swiper-wrapper">        

                <?php while( have_rows('utlatanden') ): the_row(); ?>
                    <div class="swiper-slide">
                        <div class="inside-wrapper">
                            <?php
                            $image = get_sub_field('logo');
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)
                            if( $image ) {
                                echo wp_get_attachment_image( $image, $size, '', array( "alt" => get_sub_field('foretagorganisation') ) );
                            } elseif (get_sub_field('foretagorganisation')) {
                                echo '<h5>'.get_sub_field('foretagorganisation').'</h5>';
                            }
                            ?>
                            <blockquote><p><?php the_sub_field('utlatande'); ?></p></blockquote>
                            <?php if(get_sub_field('fornamn_efternamn')) { ?>
                                <small>
                                    <?php
                                        $image = get_sub_field('personbild');
                                        $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
                                        if( $image ) {
                                            echo wp_get_attachment_image( $image, $size, '', array( "alt" => get_sub_field('fornamn_efternamn') ) );
                                        }
                                    ?>
                                    <?php the_sub_field('fornamn_efternamn');?>
                                    <br>
                                    <?php if(get_sub_field('titel')){
                                        echo get_sub_field('titel').', ';
                                    } ?>
                                    <?php the_sub_field('foretagorganisation');?>
                                </small>
                            <?php } ?>
                        </div>
                    </div>
                <?php endwhile; ?>
                
            </div>
        </div>

    <script>
      var swiper = new Swiper(".swiper-<?php echo esc_attr($id); ?>", {
        slidesPerView: 1,
        spaceBetween: 1,
        breakpoints: {
            850: {
            slidesPerView: 2,
            spaceBetween: 1
        },
        }
      });
    </script>

    <?php endif; ?>

</div>