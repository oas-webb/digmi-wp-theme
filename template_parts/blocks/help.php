<?php
    $help = get_field('help');
    $faq_items = $help['questions_answers'];
    $faq_links = $help['help_documents'];
    $buttons = $help['buttons']['button'];
?>

<div class="big-wrapper">
    <div class="outer faq">
        <div class="wrapper">
            <div class="inner">
                <h2 class="h2"><?php echo $help['heading']; ?></h2>
                <div class="faq-row">
                    <div class="column">
                        <?php echo $help['text']; ?>
                        <?php if($buttons[0]): ?>
                            <div class="button-wrapper">
                                <?php foreach( $buttons as $button ): ?>
                                    <a target="<?php echo $button['link']['target']; ?>" href="<?php echo $button['link']['url']; ?>" class="button <?php echo $button['variant']; ?>">
                                        <span class="<?php echo $button['color']; ?>" ><?php echo $button['link']['title']; ?></span>
                                        <?php if($button['icon'] === true): ?>
                                            <svg width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="<?php echo $button['icon_color']; ?>" stroke-width="0.25"/>
                                            </svg>
                                        <?php endif; ?>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>


                    </div>
                    <div class="column">
                        <h3>Vanliga frågor</h3>
                        <div class="accordion">
                            <?php foreach( $faq_items as $faq_item ): ?>
                                <div class="a-container">
                                    <p class="a-btn">
                                        <?php echo $faq_item['question']; ?>
                                        <svg width="17" height="9" viewBox="0 0 17 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.6667 1L8.66675 8L1.66675 1" stroke="#F19B06" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </p>
                                    <div class="a-panel"><?php echo $faq_item['answer']; ?></div>
                                </div>
                            <?php endforeach; ?>
                            <div class="button-wrapper">
                                <a href="" class="button grey">
                                    <span>Visa alla vanliga frågor</span>
                                    <svg width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="black" stroke-width="0.25"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <h3>Hjälp och dokument</h3>
                        <ul>
                            <?php foreach( $faq_links as $faq_link ): ?>
                                <li>
                                    <a href="<?php echo $faq_link['link']['url'] ?>">
                                        <span><?php echo $faq_link['link']['title'] ?></span>
                                        <svg width="13" height="12" viewBox="0 0 13 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="black" stroke="none" stroke-width="0.25"/>
                                        </svg>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>