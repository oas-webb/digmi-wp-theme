<?php
    $video = get_field('video');
?>

<div id="video_wrapper" class="big-wrapper block-wrapper">
    <div class="outer video-wrapper">
        <div class="wrapper">
            <div class="inner">
                <h2 class="h2"><?php echo $video['heading']; ?></h2>
                <?php if($video['video']): ?>
                    <h2 class="h1"><?php echo $video['video']; ?></h2>
                <?php endif; ?>
                <?php if($video['video_url']): ?>
                    <div class="iframe-wrapper">
                        <?php echo $video['video_url']; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>