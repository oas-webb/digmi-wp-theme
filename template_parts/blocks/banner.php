<?php
    $banner = get_field('banner');
    // var_dump($banner);
?>

<div id="banner_wrapper" class="big-wrapper block-wrapper">
    <div class="outer background-black">
        <div class="wrapper">
            <div class="inner">
                <div id="banner-wrapper">
                    <div class="banner-row">
                        <div class="column">
                            <img src="<?php echo $banner['column_1_icon']['url']; ?>" alt="<?php echo $banner['column_1_icon']['alt']; ?>">
                            <div class="text">
                                <?php echo $banner['column_1']; ?>
                            </div>
                        </div>
                        <div class="column">
                            <img src="<?php echo $banner['column_2_icon']['url']; ?>" alt="<?php echo $banner['column_2_icon']['alt']; ?>">
                            <div class="text">
                                <?php echo $banner['column_2']; ?>
                            </div>
                        </div>
                        <div class="column">
                            <img src="<?php echo $banner['column_3_icon']['url']; ?>" alt="<?php echo $banner['column_3_icon']['alt']; ?>">
                            <div class="text">
                                <?php echo $banner['column_3']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>