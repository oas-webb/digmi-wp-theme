<?php
    $image_text_special = get_field('image_text_special');
?>
<div id="image_text_special_wrapper" class="block-wrapper <?php echo $image_text_special['background_color']; ?>">
    <div class="wrapper">
        <div class="inner">
            <div class="col left">
                <div class="inner-left">
                    <?php //var_dump($image_text_special['content_1']); ?>
                    <?php //var_dump($image_text_special['content_2']); ?>


                    <?php if($image_text_special['content_1']['heading']): ?>
                        <div class="content-1-wrapper">
                            <h2 class="h1"><?php echo $image_text_special['content_1']['heading']; ?></h2>
                            <div class="text-wrapper">
                                <?php echo $image_text_special['content_1']['text']; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if($image_text_special['content_2']['heading']): ?>
                        <div class="content-2-wrapper">
                            <h2 class="h2"><?php echo $image_text_special['content_2']['heading']; ?></h2>
                            <div class="text-wrapper">
                                <?php echo $image_text_special['content_2']['text']; ?>
                            </div>
                        </div>
                    <?php endif; ?>


                </div>
            </div>
            <?php //var_dump($image_text_special['image']['url']); ?>
            <?php if($image_text_special['image']['url']): ?>
                <div class="col right">
                    <img class="<?php echo $image_text_special['rounded_corners']; ?>" src="<?php echo $image_text_special['image']['url']; ?>" alt="<?php echo $image_text_special['image']['alt']; ?>">
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>