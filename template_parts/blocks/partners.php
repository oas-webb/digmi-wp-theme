<?php
    $logotypes = get_field('logotypes');
?>
<div id="partners_wrapper" class="block-wrapper">

            <div class="logotypes-wrapper">
                <?php foreach( $logotypes as $logo ): ?>
                    <img src="<?php echo $logo['logo']['url']; ?>" alt="<?php echo $logo['logo']['alt']; ?>">
                <?php endforeach; ?>
            </div>

</div>