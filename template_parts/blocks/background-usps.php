<?php
    $background_usps = get_field('usps_background');
    $usps = $background_usps['usps']['usps'];
    // var_dump($background_usps);
?>
<div id="background_usps_wrapper" class="block-wrapper" style="background: url(<?php echo $background_usps['image']['url']; ?>) no-repeat center;">
    <?php if($background_usps['usps']['usps']): ?>
        <div class="wrapper">
            <div class="inner">
                <h3><?php echo $background_usps['pre_heading']; ?></h3>
                <h2 class="h1"><?php echo $background_usps['heading']; ?></h2>
                <div class="usps-wrapper">
                    <?php foreach( $usps as $usp ): ?>
                        <div class="usp">
                            <strong><?php echo $usp['heading'] ?></strong>
                            <?php echo $usp['text'] ?>
                            <?php if($usp['buttons']['button']): ?>
                                <div class="button-wrapper">
                                    <a class="button <?php echo $button['variant']; ?>" href="/">
                                        <span class="<?php echo $button['color']; ?>">Normal</span>
                                        <svg width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="<?php echo $button['icon_color']; ?>" stroke-width="0.25"/>
                                        </svg>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>