<?php
    $quote = get_field('quote');
?>

<div id="quote_wrapper" class="block-wrapper">
    <div class="wrapper">
        <div class="inner">
            <div class="quote-inner">
                <div class="dots-wrapper">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/dot-pattern.svg" alt="dot-pattern">
                </div>
                <div class="image-wrapper">
                    <img src="<?php echo $quote['image']['url']; ?>" alt="<?php echo $quote['image']['alt']; ?>">
                </div>
                <div class="meta-wrapper">
                    <div class="quote-icon">
                        <svg width="144" height="144" viewBox="0 0 144 144" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M41.485 15C17.753 31.753 1 59.208 1 89.455C1 114.119 15.891 128.545 33.109 128.545C49.396 128.545 61.495 115.515 61.495 100.158C61.495 84.802 50.792 73.634 36.832 73.634C34.04 73.634 30.317 74.099 29.386 74.564C31.713 58.743 46.604 40.129 61.496 30.822L41.485 15ZM121.525 15C98.257 31.753 81.505 59.208 81.505 89.455C81.505 114.119 96.396 128.545 113.614 128.545C129.436 128.545 142 115.515 142 100.158C142 84.802 130.832 73.634 116.871 73.634C114.079 73.634 110.822 74.099 109.891 74.564C112.218 58.743 126.644 40.129 141.535 30.822L121.525 15Z" stroke="#F19B06" stroke-opacity="0.2" stroke-width="2"/>
                        </svg>
                    </div>
                    <p><?php echo $quote['text']; ?></p>
                    <h5><?php echo $quote['name']; ?></h5>
                    <h5 class="text-orange"><?php echo $quote['title']; ?></h5>
                </div>
            </div>
        </div>
    </div>
</div>