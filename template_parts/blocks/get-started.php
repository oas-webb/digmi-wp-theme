<?php
    $get_started = get_field('get_started');
    $buttons = $get_started['buttons']['button'];
    $buttons_usp_1 = $get_started['usp_1']['buttons']['button'];
    $buttons_usp_2 = $get_started['usp_2']['buttons']['button'];
?>

<div id="get_started_wrapper" class="big-wrapper block-wrapper">
    <div class="outer get-started-wrapper">
        <div class="wrapper">
            <div class="inner">

                <h2 class="h1"><?php echo $get_started['heading']; ?></h2>
                <div class="column">   
                    <?php echo $get_started['text']; ?>
                    <?php if( $buttons[0] ): ?>
                        <div class="button-wrapper">
                            <?php foreach( $buttons as $button ): ?>
                                <a target="<?php echo $button['link']['target']; ?>" href="<?php echo $button['link']['url']; ?>" class="button <?php echo $button['variant']; ?>">
                                    <span class="<?php echo $button['color']; ?>"><?php echo $button['link']['title']; ?></span>
                                    <?php if( $buttons[0]['icon'] === true ): ?>
                                        <svg width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="<?php echo $button['icon_color']; ?>" stroke-width="0.25"/>
                                        </svg>
                                    <?php endif; ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="column">
                    <div class="usp-1-wrapper">
                        <h4><?php echo $get_started['usp_1']['heading']; ?></h4>
                        <?php echo $get_started['usp_1']['text']; ?>
                        <div class="button-wrapper">
                            <?php foreach( $buttons_usp_1 as $button_usp_1 ): ?>
                                <a target="<?php echo $button_usp_1['link']['target']; ?>" href="<?php echo $button_usp_1['link']['url']; ?>" class="button <?php echo $button_usp_1['variant']; ?>">
                                    <span class="<?php echo $button_usp_1['color']; ?>"><?php echo $button_usp_1['link']['title']; ?></span>
                                    <svg width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="<?php echo $button['icon_color']; ?>" stroke-width="0.25"/>
                                    </svg>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="usp-2-wrapper">
                        <h4><?php echo $get_started['usp_2']['heading']; ?></h4>
                        <?php echo $get_started['usp_2']['text']; ?>
                        <div class="button-wrapper">
                            <?php foreach( $buttons_usp_2 as $button_usp_2 ): ?>
                                <a target="<?php echo $button_usp_2['link']['target']; ?>" href="<?php echo $button_usp_2['link']['url']; ?>" class="button <?php echo $button_usp_2['variant']; ?>">
                                    <span class="<?php echo $button_usp_2['color']; ?>"><?php echo $button_usp_2['link']['title']; ?></span>
                                    <svg width="13" height="12" viewBox="0 0 13 12" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M7.52099 0.436754L7.52085 0.43662C7.17954 0.0953047 6.62789 0.0953046 6.28658 0.43662C5.94527 0.777924 5.94526 1.32954 6.28654 1.67086C6.28655 1.67087 6.28656 1.67089 6.28658 1.6709L9.72656 5.12501H1.64996C1.16843 5.12501 0.774963 5.51847 0.774963 6.00001C0.774963 6.48154 1.16843 6.87501 1.64996 6.87501H9.72569L6.27907 10.3216C5.93776 10.6629 5.93776 11.2221 6.27907 11.5634C6.62039 11.9047 7.17204 11.9047 7.51335 11.5634L12.4559 6.6209C12.7972 6.27958 12.7972 5.72794 12.4559 5.38662L7.52099 0.436754Z" fill="<?php echo $button['icon_color']; ?>" stroke-width="0.25"/>
                                    </svg>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                <?php //echo $get_started['button']; ?>
            </div>
        </div>
    </div>
</div>