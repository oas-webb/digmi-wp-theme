<div class="big-wrapper book-demo">
    <div class="outer">
        <div class="wrapper">
            <div class="inner">
                <div class="book-demo-iframe-wrapper">
                    <iframe id="bookDemo" src='https://outlook.office365.com/owa/calendar/BokasupportBooksupport@digmi.se/bookings/' width='1500' height='2020' frameborder="0" scrolling="yes"></iframe>
                    <div class="button-wrapper">
                        <a class="button primary" target="_blank" href='https://outlook.office365.com/owa/calendar/BokasupportBooksupport@digmi.se/bookings/'>
                            <span>Öppna tidsbokningen i nytt fönster</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>