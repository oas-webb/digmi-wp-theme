<?php
    $columns_list = get_field('columns_list');
    $columns = $columns_list['columns'];
?>

<div id="columns_list_wrapper" class="big-wrapper block-wrapper">
    <div class="outer get-started-wrapper">
        <div class="wrapper">
            <div class="inner">
                <div class="columns-wrapper">
                    <?php foreach($columns as $column): ?>
                            <div class="column">
                                <h3><?php echo $column['heading']; ?></h3>
                                <ul>
                                    <?php foreach($column['list'] as $list_item): ?>
                                        <li>
                                            <h5>
                                                <?php echo $list_item['list_heading']; ?>
                                            </h5>
                                            <p>
                                                <?php echo $list_item['more_info']; ?>
                                            </p>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>