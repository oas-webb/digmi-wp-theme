<?php

function digmi_deal_func( $atts ){
    ob_start();
    global $post;
    $current_id = $post->ID;
?>

<div class="digmi-wrapper">
    <?php if(get_field('punchline',$current_id) || get_field('erbjudanden',$current_id)) :?>
        <div class="offer_inforamtion alignfull has-lightgrey-background-color">
            <div class="inside-wrapper">
                <h1>
                    <span class="h4"><?php echo get_the_title($current_id); ?></span>
                    <?php if(get_field('punchline',$current_id)) { ?><span class="h1"><?php the_field('punchline',$current_id) ?></span><?php } ?>
                </h1>


                <?php if( have_rows('erbjudanden',$current_id) ): ?>
                    <ul class="offers">
                    <?php while( have_rows('erbjudanden',$current_id) ): the_row();

                        if(get_sub_field('visa') == 'summary') {
                            continue;
                        }

                        $image = get_sub_field('bildikon');
                        ?>
                        <li>
                            <?php echo wp_get_attachment_image( $image, 'thubmail' ); ?>
                            <h2 class="h4"><?php the_sub_field('rubrik'); ?></h2>
                            <p><?php the_sub_field('beskrivning'); ?> Normalt <?php the_sub_field('ord_pris'); ?> kr/månad.</p>

                        </li>
                    <?php endwhile; ?>
                    </ul>
                <?php endif; ?>


            </div>
        </div>



        <div class="choice_information alignfull has-black-background-color has-white-color">
            <div class="inside-wrapper">
                <h2 class="h1 has-white-color align-center ">Speciella priser för dig</h2>
                <p class="has-digmicopy-neg-color align-center ">Tydliga paket med fasta priser och utan dolda avgifter.</p>

<div class="pricing-tiers">
    <div class="recommended">
        <h5><?php echo get_the_title($current_id); ?></h5>
        <div class="price-wrapper">
            <div class="price h2 has-black color"><span id="showprice"></span> kr</div>
            <div class="price-info">
                <span class="has-black-color">enhet / månad</span>
                <span class="has-digmicopy-color">faktureras kvartalsvis</span>
            </div>
        </div>
        <div class="units-wrapper">
        <?php
                    // Check rows exists.
                    if( have_rows('priser','option') ):
                        $min = 999;
                        $max = 0;
                        while( have_rows('priser','option') ) : the_row();

                        $fran = (int) get_sub_field('fran');
                        $till = (int) get_sub_field('till');

                            if($fran < $min) {
                                $min = $fran;
                            }
                            if($till > $max) {
                                $max = $till;
                            }
                        endwhile;

                        echo '<select name="units" id="units">';
                        $index = 0;
                        foreach (range($min, $max) as $units) {
                            if($index == 2) {
                                echo '<option value="'.$units.'" selected>'.$units.' st</option>';
                            } else {
                                echo '<option value="'.$units.'">'.$units.' st</option>';
                            }
                            $index++;
                        }
                        echo '</select>';
                    endif;

                ?>



            <label for="units">enheter</label>
        </div>
        <button class="get-recommended">Köp standard</button>
        <ul class="usp has-black-color">
            <li>Färdiginstallerad läsplatta</li>
            <li>Tidningsprenumeration</li>
            <li>Bokningssystem</li>
            <li>Standardapplikationer, Office 365, Loreal-appar</li>
            <li>Försäkring mot stöld och skada (självrisk 2000 kr)</li>

        </ul>
    </div>
    <div class="custom">
        <h5 class="has-white-color">Anpassad</h5>
        <p class="has-white-color">Skapa ett paket anpassat för er verksamhet.<br>
            <span class="has-digmicopy-neg-color">Tillgängligt för företag med stora volymer eller unika affärsmodeller.</span>
        </p>
        <a href="https://digmi.se/kontakt/" target="_blank" class="get-custom">Kontakta säljteamet</a>
    </div>
</div>

            </div>
        </div>

    <?php endif; ?>
</div>

<?php
    return ob_get_clean();
}
add_shortcode( 'digmi_deal', 'digmi_deal_func' );