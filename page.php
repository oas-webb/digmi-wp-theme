<?php
    get_header();
    $book_support = get_field('book_support');
    $book_support_link = get_field('book_support_link');
?>
    <main>
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php endif; ?>

    </main>

    <?php if($book_support === true): ?>
        <a href="<?php echo $book_support_link['url']; ?>" class="book-support-btn">
            <span><?php echo $book_support_link['title']; ?></span>
        </a>
    <?php endif; ?>

<?php get_footer(); ?>