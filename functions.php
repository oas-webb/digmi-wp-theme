<?php


// Digmi shortcode
// require_once '/functions/digmi-shortcode.php';

/**
 * Removes comments
*/
// from admin menu
add_action('admin_menu', 'my_remove_admin_menus');
function my_remove_admin_menus(){
  remove_menu_page('edit-comments.php');
}
//from post and pages
add_action('init', 'remove_comment_support', 100);
function remove_comment_support(){
  remove_post_type_support('post', 'comments');
  remove_post_type_support('page', 'comments');
}
//from admin bar
function mytheme_admin_bar_render(){
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu('comments');
}
add_action('wp_before_admin_bar_render', 'mytheme_admin_bar_render');


/**
 * Fix filenames on upload
 */
function femp_sanitize_filename_on_upload($filename){
  $exp = explode('.', $filename);
  $ext = end($exp);
  $sanitized = preg_replace(
    '/[^a-zA-Z0-9-_.]/',
    '',
    substr($filename, 0, -(strlen($ext) + 1))
  );
  $sanitized = str_replace('.', '-', $sanitized);
  return strtolower($sanitized . '.' . $ext);
}
add_filter('sanitize_file_name', 'femp_sanitize_filename_on_upload', 10);


// Add styles and scripts

function oas_scripts() {

  // CSS
  wp_enqueue_style ( 'calendly', 'https://assets.calendly.com/assets/external/widget.css' );

    $filename = get_template_directory() . '/style.css';
    $filtid = date ("YmdHis", filemtime($filename));
    wp_enqueue_style ( 'oas_styles', get_template_directory_uri() . '/style.css', array(), $filtid );

  wp_enqueue_style ( 'oas_fonts_harpers', get_template_directory_uri() . '/assets/fonts/fonts_harpersGrotesque.css' );
  wp_enqueue_style ( 'oas_fonts_roboto', get_template_directory_uri() . '/assets/fonts/fonts_roboto.css' );
  wp_enqueue_style ( 'oas_styles_swiper', 'https://unpkg.com/swiper@8/swiper-bundle.min.css' );

  // JS
  wp_enqueue_script('jquery');
  wp_enqueue_script ( 'calendly', 'https://assets.calendly.com/assets/external/widget.js' );
  wp_enqueue_script ( 'oas_scripts_swiper', 'https://unpkg.com/swiper@8/swiper-bundle.min.js' );
  wp_enqueue_script ( 'scrollreveal', 'https://unpkg.com/scrollreveal' );
  wp_enqueue_script ( 'balancetext', 'https://cdnjs.cloudflare.com/ajax/libs/balance-text/3.3.0/balancetext.min.js' );
  

    $filename = get_template_directory() . '/assets/js/custom.js';
    $filtid = date ("YmdHis", filemtime($filename));
    wp_enqueue_script ( 'oas_scripts', get_template_directory_uri() . '/assets/js/custom.js',array(),$filtid);
    wp_localize_script( 'oas_scripts', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );



}
add_action('wp_enqueue_scripts', 'oas_scripts');

// Add menu
function digmi_menu() {
  register_nav_menu('digmi_menu',__( 'Digmi menu' ));
  register_nav_menu('footer_menu',__( 'Footer menu' ));
  register_nav_menu('footer_menu_2',__( 'Footer menu 2' ));
}
add_action( 'init', 'digmi_menu' );


add_theme_support('align-wide');
add_theme_support('disable-custom-colors');

    // Editor Color Palette
    add_theme_support( 'editor-color-palette', array(
      array(
          'name'  => __( 'Orange', 'digmi' ),
          'slug'  => 'orange',
          'color'	=> '#f19b06',
      ),
      array(
        'name'  => __( 'Copy color', 'digmi' ),
        'slug'  => 'copy',
        'color'	=> '#474b55',
      ),
      array(
        'name'  => __( 'Light gray', 'digmi' ),
        'slug'  => 'lightgray',
        'color'	=> '#E9E9EA',
      ),
      array(
          'name'  => __( 'Black', 'digmi' ),
          'slug'  => 'black',
          'color'	=> '#24262b',
      ),
      array(
          'name'  => __( 'White', 'digmi' ),
          'slug'  => 'white',
          'color'	=> '#ffffff',
      ),
  ) );


  // Register ACF blocks
if (function_exists('acf_register_block_type')) {
  add_action('acf/init', 'register_acf_block_types');
}

// Custom block categories
function amtele_blocks( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'digmi-blocks',
				'title' => 'Digmi block',
			),
		)
	);
}
add_filter( 'block_categories_all', 'amtele_blocks', 10, 2);

function register_acf_block_types() {

  acf_register_block_type (
    array(
      'name' => 'digmi-testimonial',
      'title' => __('Kundutlåtande'),
      'description' => __('Block fört kundutlåtande'),
      'render_template' => 'template_parts/blocks/digmi-testimonial.php',
      'category' => 'digmi-blocks',
      'mode' => 'auto'
    )
  );

  acf_register_block_type (
    array(
      'name' => 'image_text',
      'title' => __('Bild & Text'),
      'description' => __('Bild & text block'),
      'render_template' => 'template_parts/blocks/image-text.php',
      'keywords' => array('bild', 'text', 'cta'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'image_text_special',
      'title' => __('Bild & Text special'),
      'description' => __('Bild & text special block'),
      'render_template' => 'template_parts/blocks/image-text-special.php',
      'keywords' => array('bild', 'text', 'cta'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'partners',
      'title' => __('Partners'),
      'description' => __('Partners block'),
      'render_template' => 'template_parts/blocks/partners.php',
      'keywords' => array('partners'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'background_usps',
      'title' => __('Uspar med bakgrund'),
      'description' => __('Uspar block'),
      'render_template' => 'template_parts/blocks/background-usps.php',
      'keywords' => array('uspar'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'book_demo',
      'title' => __('Boka demo iframe'),
      'description' => __('Boka demo block'),
      'render_template' => 'template_parts/blocks/book-demo.php',
      'keywords' => array('demo', 'book'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'digmi_table',
      'title' => __('Digmi tabell'),
      'description' => __('Digmi tabell'),
      'render_template' => 'digmi-table.php',
      'keywords' => array('table'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'digmi_offer',
      'title' => __('Digmi erbjudande'),
      'description' => __('Digmi erbjudande'),
      'render_template' => 'template_parts/blocks/digmi-offer.php',
      'keywords' => array('offer', 'form'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'banner',
      'title' => __('Banderoll'),
      'description' => __('Banderoll'),
      'render_template' => 'template_parts/blocks/banner.php',
      'keywords' => array('information', 'banderoll'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'faq',
      'title' => __('FAQ'),
      'description' => __('FAQ'),
      'render_template' => 'template_parts/blocks/faq.php',
      'keywords' => array('faq'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'text-cta',
      'title' => __('Text CTA'),
      'description' => __('FAQ'),
      'render_template' => 'template_parts/blocks/text-cta.php',
      'keywords' => array('cta', 'text'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'video',
      'title' => __('Video'),
      'description' => __('Block med rubrik och video'),
      'render_template' => 'template_parts/blocks/video.php',
      'keywords' => array('video'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'get_started',
      'title' => __('Kom igång'),
      'description' => __('Block med kom igång uppmaning'),
      'render_template' => 'template_parts/blocks/get-started.php',
      'keywords' => array('Kom igång'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'quote',
      'title' => __('Citat'),
      'description' => __('Block med citat'),
      'render_template' => 'template_parts/blocks/quote.php',
      'keywords' => array('Citat'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'columns_list',
      'title' => __('Kolumner med lista'),
      'description' => __('Block med kolumner och lista'),
      'render_template' => 'template_parts/blocks/columns-list.php',
      'keywords' => array('Kolumner', 'Lista'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'help',
      'title' => __('Behöver du hjälp-blocket'),
      'description' => __('Block med info och hjälpreda'),
      'render_template' => 'template_parts/blocks/help.php',
      'keywords' => array('Hjälp', 'FAQ'),
      'category' => 'digmi-blocks',
      'mode' => 'edit'
    )
  );
}

// Add options pages
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title' 	=> 'Priser',
    'menu_title'	=> 'Priser',
    'menu_slug' 	=> 'prices-general-settings',
    'capability'	=> 'edit_posts',
    'redirect'		=> false
  ));
}


/**
 * Spara acf-json sync
 */
function my_acf_json_save_point($path){
  // update path
  $path = get_stylesheet_directory() . '/acf-json';
  return $path;
}
add_filter('acf/settings/save_json', 'my_acf_json_save_point');

// Ladda acf-json
function my_acf_json_load_point($paths){
  unset($paths[0]);
  $paths[] = get_stylesheet_directory() . '/acf-json';
  return $paths;
}
add_filter('acf/settings/load_json', 'my_acf_json_load_point');





/**
 * Favicons
 */
function oas_favicons(){
  ?>
  <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('stylesheet_directory'); ?>/assets/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('stylesheet_directory'); ?>/assets/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('stylesheet_directory'); ?>/assets/favicons/favicon-16x16.png">
  <link rel="manifest" href="<?php bloginfo('stylesheet_directory'); ?>/assets/favicons/site.webmanifest">
  <link rel="mask-icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/favicons/safari-pinned-tab.svg" color="#f19b06">
  <meta name="apple-mobile-web-app-title" content="Digmi">
  <meta name="application-name" content="Digmi">
  <meta name="msapplication-TileColor" content="#f19b06">
  <meta name="theme-color" content="#ffffff">
<?php
}
add_filter('wp_head', 'oas_favicons');

/**
 * AJAX
*/
function get_price() {
  $units = $_POST[units];

if( have_rows('priser','option') ):
  while( have_rows('priser','option') ) : the_row();
      $fran = (int)get_sub_field('fran');
      $till = (int)get_sub_field('till');

      if($units >= $fran && $units <= $till) {
        echo get_sub_field('pris');
      }
  endwhile;
endif;

  wp_die();
}

add_action( 'wp_ajax_nopriv_get_price', 'get_price' );
add_action( 'wp_ajax_get_price', 'get_price' );



/**
 * Removes ul from wp_nav_menu
 */
function remove_ul($menu) {
  return preg_replace(['#^<ul[^>]*>#', '#</ul>$#'], '', $menu);
}
add_filter('wp_nav_menu', 'remove_ul');
